# frozen_string_literal: true

require_relative 'scene'
require_relative '../systems/drawing_system'

# The main menu
class MenuScene < Scene
  def initialize(entity_manager)
    super('menu', entity_manager, [], [DrawingSystem.new(self)])

    background = @entity_manager.create_tagged_entity('background')
    @entity_manager.add_component(background, Drawable.new(Gosu::Image.new('shibe3.gif')))
    @entity_manager.add_component(background, Spatial.new(0, 0, 0))
  end

  def scene_start
    puts 'scene starting'
  end

  def scene_end
    puts 'scene ending'
  end
end
