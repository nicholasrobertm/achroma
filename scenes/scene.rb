# frozen_string_literal: true

# A base scene
class Scene

  attr_reader :name

  def initialize(name, entity_manager, process_systems = [], draw_systems = [])
    @name = name
    @entity_manager = entity_manager
    @process_systems = process_systems
    @draw_systems = draw_systems
  end

  def update
    @process_systems.each do |process_system|
      process_system.process_one_game_tick(@entity_manager)
    end
  end

  def draw
    @draw_systems.each do |draw_system|
      draw_system.process_one_game_tick(@entity_manager)
    end
  end

  def scene_start
    raise 'You must implement scene_start'
  end

  def scene_end
    raise 'You must implement scene_end'
  end

end
