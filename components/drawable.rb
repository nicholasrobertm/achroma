# frozen_string_literal: true

require_relative 'component'

# A drawable component
class Drawable < Component

  attr_reader :image

  def initialize(image, options = {})
    super()
    @image = image
  end

end