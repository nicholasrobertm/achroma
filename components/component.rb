# frozen_string_literal: true

require 'securerandom'

# Base component
class Component
  def initialize
    @id = SecureRandom.uuid
  end

  def to_s
    "Component #{id}: #{self.class.name}"
  end
end
