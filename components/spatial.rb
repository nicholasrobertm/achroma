# frozen_string_literal: true

# For things that have a position in the world
class Spatial < Component

  attr_accessor :x, :y, :z

  def initialize(x, y, z)
    super()
    @x = x
    @y = y
    @z = z
  end

end
