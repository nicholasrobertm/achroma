# frozen_string_literal: true

require 'securerandom'

# Manages all the entities
class EntityManager

  attr_accessor :game
  attr_reader :id

  def initialize(game)
    @id = SecureRandom.uuid
    @ids_to_tags = {}
    @tags_to_ids = {}
    @component_stores = Hash.new { |h, k| h[k] = {} }
  end

  def all_entities
    @ids_to_tags.keys
  end

  def create_entity
    uuid = SecureRandom.uuid
    @ids_to_tags[uuid] = '-'
    uuid
  end

  def create_tagged_entity(tag)
    raise 'A tag must be specified when creating a tagged entity' if tag.nil?
    raise ArgumentError, 'Tag - is reserved and cannot be used' if tag == '-'

    uuid = create_entity
    @ids_to_tags[uuid] = tag

    if @tags_to_ids.has_key? tag
      @tags_to_ids[tag] << uuid
    else
      @tags_to_ids[tag] = [uuid]
    end
    uuid
  end

  def get_entities_with_tag(tag)
    @tags_to_ids[tag]
  end

  def get_tag(uuid)
    raise 'UUID must be specified' if uuid.nil?

    @ids_to_tags[uuid]
  end

  def component?(entity, component)
    raise 'UUID and component must be specified' if entity.nil? || component.nil?

    store = @component_stores[component.class]
    if store.nil?
      false # NOBODY has this component type
    else
      store.key?(entity) && store[entity].include?(component)
    end
  end

  def component_of_type?(entity, component_class)
    raise 'UUID and component class must be specified' if entity.nil? || component_class.nil?

    store = @component_stores[component_class]
    if store.nil?
      false # NOBODY has this component type
    else
      store.key?(entity) && store[entity].size.positive?
    end
  end

  def add_component(entity, component)
    raise 'UUID and component must be specified' if entity.nil? || component.nil?

    store = @component_stores[component.class]
    if store.nil?
      store = {}
      @component_stores[component.class] = store
    end

    if store.key? entity
      store[entity] << component unless store[entity].include? component
    else
      store[entity] = [component]
    end
  end

  def add_singleton_component(entity, component)
    return false if component_of_type?(entity, component.class.to_s.to_sym)

    add_component(entity, component)
  end

  def remove_component(entity, component)
    raise 'UUID and component must be specified' if entity.nil? || component.nil?

    store = @component_stores[component.class]
    return nil if store.nil?

    components = store[entity]
    return nil if components.nil?

    result = components.delete(component)
    raise "Entity #{entity} did not possess #{component} to remove" if result.nil?

    store.delete(entity) if store[entity].empty?
    true
  end

  def get_component_of_type(entity, component_class)
    raise 'UUID and component class must be specified' if entity.nil? || component_class.nil?

    # return nil unless has_component_of_type?(entity, component.class)
    store = @component_stores[component_class]
    return nil if store.nil?

    components = store[entity]
    return nil if components.nil? || components.empty?

    if components.size != 1
      puts "Warning: you probably expected #{entity} to have just one #{component_class} but it had #{components.size}."
    end

    components.first
  end

  def get_all_components_of_type(component_sym)
    Set.new(@component_stores[component_sym].values).flatten
  end

  def get_all_components(entity)
    raise 'UUID must be specified' if entity.nil?

    components = []
    @component_stores.values.each do |store|
      components += store[entity] if store[entity]
    end
    components
  end

  def get_all_entities_with_component_of_type(component_class)
    raise 'Component class must be specified' if component_class.nil?

    store = @component_stores[component_class]
    if store.nil?
      []
    else
      store.keys
    end
  end

  def get_all_entities_with_components_of_type(component_classes)
    raise 'Component classes must be specified' if component_classes.nil?

    entities = all_entities
    component_classes.each do |klass|
      entities &= get_all_entities_with_component_of_type(klass)
    end
    entities
  end

  def kill_entity(entity)
    raise 'UUID must be specified' if entity.nil?

    @component_stores.each_value do |store|
      store.delete(entity)
    end
    @tags_to_ids.each_key do |tag|
      @tags_to_ids[tag].delete entity if @tags_to_ids[tag].include? entity
    end

    !@ids_to_tags.delete(entity).nil?
  end

  def dump_details
    output = to_s
    all_entities.each do |e|
      output << "\n #{e} (#{@ids_to_tags[e]})"
      comps = get_all_components(e)
      comps.each do |c|
        output << "\n   #{c}"
      end
    end
    output
  end

  def to_s
    "EntityManager {#{@id}: #{all_entities.size} managed entities}"
  end

end
