# frozen_string_literal: true

require_relative 'entity_manager'
require_relative 'scene_manager'

require_relative 'scenes/menu_scene'

require 'gosu'

# Main application class, this is where it all starts
class App < Gosu::Window
  def initialize
    super 800, 600
    self.caption = 'Achroma'

    @entity_manager = EntityManager.new(self)
    @scene_manager = SceneManager.new('menu', [MenuScene.new(@entity_manager)])

  end

  def update
    @scene_manager.update
  end

  def draw
    @scene_manager.draw
  end

end
App.new.show
