# frozen_string_literal: true

require_relative 'scenes/scene'

# Manages all the scenes in the game and the switching between them
class SceneManager
  def initialize(active_scene = '', scenes = [])
    @active_scene = active_scene
    @scenes = scenes
    switch_scene(active_scene)
  end

  def switch_scene(name)
    raise "There is no scene with name #{name}" unless scene?(name)

    # End the last scene
    get_scene(@active_scene).name
    get_scene(@active_scene).scene_end

    # Start the new scene
    @active_scene = name
    get_scene(@active_scene).scene_start
  end

  def update
    get_scene(@active_scene).update
  end

  def draw
    get_scene(@active_scene).draw
  end

  def scene?(name)
    @scenes.any? { |s| s.name == name }
  end

  def get_scene(name)
    @scenes.select { |scene| scene.name == name }[0]
  end

end
