# frozen_string_literal: true

# Base System
class System
  attr_reader :game

  def initialize(game)
    @game = game
  end

  def process_one_game_tick
    raise 'You must override this function in each system'
  end

end
