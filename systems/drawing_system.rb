# frozen_string_literal: true

require_relative 'system'
require_relative '../components/drawable'
require_relative '../components/spatial'

# Handles drawing things in the game
class DrawingSystem < System
  def process_one_game_tick(entity_manager)
    entities = entity_manager.get_all_entities_with_components_of_type([Drawable, Spatial])

    entities.each do |e|
      spatial_component = entity_manager.get_component_of_type(e, Spatial)
      drawable_component = entity_manager.get_component_of_type(e, Drawable)

      drawable_component.image.draw(spatial_component.x, spatial_component.y, spatial_component.z)

    end
  end
end
